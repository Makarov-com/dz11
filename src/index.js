import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
// import Bascket from './Bascket';
import {store} from "./store"
import {Provider} from 'react-redux';




ReactDOM.render(
  <Provider store={store}>
    {/* <Bascket />, */}
    <App />
  </Provider>,
  document.getElementById('root')
  
);

