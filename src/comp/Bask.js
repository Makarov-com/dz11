import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import "../index.css"
import App from '../App';
import { cartInner } from '../store/reducers/reducersProducts';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faShoppingBasket } from '@fortawesome/free-solid-svg-icons'
import { faDoorClosed } from '@fortawesome/free-solid-svg-icons'

function Bask(props) {
    const [open, setOpen] = useState(false)
    const inner = useSelector(cartInner);
    const [value, setValue] = useState(1)
    return (
        <>
            <div>
                <button class="buttonBask" onClick={() => setOpen(true)}><FontAwesomeIcon icon={faShoppingBasket} class="buttonBask" /></button>
            </div>
            <div class="backBask" style={open ? { display: 'block' } : { display: "none" }}>
                <div className='bask' >
                    <button class="btnBask" onClick={() => setOpen(false)}>X</button>
                    {inner.map(item => {
                        return (
                            <div class="grid2">
                                <div class="photoMini"><img src={item.product.image} alt="" /></div>
                                <div class="textCart">{item.product.title}</div>
                                <div class="priceCart"><b>{item.product.price} $</b></div>
                                <div class="buttonsCart">
                                    <button class="chngBtn min" onClick={() => props.minOne(item)}> - </button>
                                    <span class="btnText">{item.count}</span>
                                    <button class="chngBtn plus" onClick={() => {
                                        props.addOne(item)
                                        // console.log(item)
                                    }}> + </button>
                                </div>
                                <button class="btnRem" onClick={() => props.delete(item)}>X</button>

                            </div>
                        )
                    })}
                </div>
            </div>


        </>
    )
}

export default Bask;