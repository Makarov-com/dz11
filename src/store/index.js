import { combineReducers } from "redux";
import {createStore, applyMiddleware} from "redux";
import reducer from './reducers/reducersProducts';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';

const rootReducer = combineReducers({
    repos: reducer,
})

export const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk)))



