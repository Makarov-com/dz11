const INIT = "INIT"
const REMOVE_FROM_CART = "REMOVE_FROM_CART"
const INCREASE_QUANTITY = "INCREASE_QUANTITY"
const DECREASE_QUANTITY = "DECREASE_QUANTITY"
const BY_AMOUNT = "BY_AMOUNT"


const defaultState = {
    products: [],
    cart: []
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case INIT:
            return {
                ...state,
                products: action.payload
            }
        case REMOVE_FROM_CART:
            return {
                ...state,
                cart: state.cart.filter(item => item.product.id !== action.payload)
                // ...state,
                // cart: state.cart.filter(item => item.product.id !== action.payload)
            }
        case BY_AMOUNT:
            if (action.payload.count > 0) {
                // && action.payload.id.length > 0
                let isNew2 = true;

                const arr = state.cart.map((item) => {
                    // console.log(item.product.id, action.payload.product);

                    if (item.product.id === action.payload.product.id) {
                        isNew2 = false;
                        // if (item.count + action.payload.count <= 100) {
                            return {
                                product: item.product,
                                count: item.count + action.payload.count
                            }
                        // }
                    }
                    return item;
                })
                if (isNew2) arr.push(action.payload);
                return {
                    ...state,
                    cart: arr
                }
            } else return {
                ...state,
                cart: [...state.cart]
            }

        case INCREASE_QUANTITY:
            if (action.payload.count > 0 && action.payload.count < 100) {
                // && action.payload.id.length > 0
                let isNew2 = true;

                const arr = state.cart.map((item) => {
                    // console.log(item.product.id, action.payload);

                    if (item.product.id === action.payload.product.id) {
                        isNew2 = false;
                        if (item.count + action.payload.count <= 100) {
                            return {
                                product: item.product,
                                count: item.count + 1
                            }
                        }
                    }
                    return item;
                })
                if (isNew2) arr.push(action.payload);
                return {
                    ...state,
                    cart: arr
                }
            } else return {
                ...state,
                cart: [...state.cart]
            }


        // case INCREASE_QUANTITY:
        //     let isNew = true;
        //     if (state.cart.length > 0) {
        //         const arr = state.cart.map((item) => {
        //             console.log(item.product.id, action.payload);
        //             if (item.product.id === action.payload.product.id) {
        //                 isNew = false;
        //                 return {
        //                     product: item.product,
        //                     count: item.count + 1
        //                 }
        //             }
        //             return item;
        //         })
        //         if (isNew) arr.push(action.payload);
        //         return {
        //             ...state,
        //             cart: arr
        //         }
        //     } else return {
        //         ...state,
        //         cart: [...state.cart, action.payload]
        //     }
        //  state.cart.map((item) => {
        //      console.log(item.product.id, action.payload.id)
        //     if (item.product.id === action.payload.product.id) {
        //         return {
        //             product: item.product,
        //             count: item.count + 1
        //         }
        //     }
        //     return item;
        // })

        //     if (state.cart.length > 0) {
        //     state.cart.map((item) => {
        //         if (item.product.id === action.payload.product.id) {
        //             return {
        //                 product: item.product,
        //                 count: item.count + 1
        //             }
        //         }
        //         return item;
        //     })
        // }
        //////////////////////////////////////////////////////////////
        // let isNew = true;
        // if (state.cart.length > 0) {
        //     const arr = state.cart.map((item) => {
        //         console.log(item.product.id, action.payload.product.id)
        //         if (item.product.id === action.payload.product.id) {
        //             isNew = false;
        //             return {
        //                 product: item.product,
        //                 count: item.count + 1
        //             }
        //         }
        //         return item;
        //     })
        //     if (isNew) arr.push(action.payload);
        //     return {
        //         ...state,
        //         cart: arr
        //     }
        // } else return {
        //     ...state,
        //     cart: [...state.cart, action.payload]
        // }

        case DECREASE_QUANTITY:

            let isExisting = false;
            const arr = state.cart.map((item) => {
                if (item.product.id === action.payload) {
                    if (item.count - 1 <= 0) {
                        isExisting = true;


                    } else return {
                        product: item.product,
                        count: item.count - 1
                    }
                }
                return item;
            })

            if (isExisting) return {
                ...state,
                cart: state.cart.filter(item => item.product.id !== action.payload)
            }

            return {
                ...state,
                cart: arr
            }


        default:
            return state
    }
}

export const cartInner = (state => state.repos.cart)
export const productsCash = (state => state.repos.items)