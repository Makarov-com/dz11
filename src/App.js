import products from './products.json';
import './index.css';
// import Bascket from './Bascket';
import { useDispatch } from "react-redux";
import axios from 'axios';
import React, { useState, useEffect } from 'react';
import Bask from "./comp/Bask"
import Warn from "./comp/Warn"


function App() {
  const dispatch = useDispatch();

  // const [value, setValue] = useState(1)
  const [value, setValue] = useState(1);
  const [warn, setWarn] = useState(false);
  const incrementValue = Number(value) || 0;



  const addItems = (item) => {
    dispatch({ type: "INIT", payload: item })
  }

  useEffect(() => {
    const fetchData = async () => {
      const response = await axios.get('https://fakestoreapi.com/products')
      addItems(response.data)
    }
    fetchData();
  }, []);


  const removeItem = (item) => {
    dispatch({ type: "DECREASE_QUANTITY", payload: item.product.id })
  }

  const onChange = (e) => {
    // var max = parseInt(e.target.max);
    // console.log(e)


    setValue(Math.abs(e.target.value))

  }




  // const addItem = (item) => {
  //   const theItem = {
  //     product: {
  //       title: item.title,
  //       id: item.id,
  //       image: item.image,
  //       price: item.price
  //     },
  //     count: item.count
  //   }
  //   dispatch({ type: "INCREASE_QUANTITY", payload: theItem })
  // }

  const addItem = (item) => {
    const theItem = {
      product: {
        "title": item.product.title,
        "id": item.product.id,
        "image": item.product.image,
        "price": item.product.price
      },
      count: Number(1)
    }
    // console.log(theItem)


    dispatch({ type: "INCREASE_QUANTITY", payload: theItem })
  }


  const byAmount = (item) => {
    const theItem = {
      product: {
        "title": item.title,
        "id": item.id,
        "image": item.image,
        "price": item.price
      },
      count: Number(value)
    }
    // console.log(theItem)

    if (parseInt(theItem.count) > 100) {
      // e.target.value = max;
      setWarn(true)

    } else {
      dispatch({ type: "BY_AMOUNT", payload: theItem })

    }

    // setValue(1)
  }


  const deleteItem = (item) => {
    // const theItem = {
    //   product: {
    //     id: item.id
    //   }
    // }
    dispatch({ type: "REMOVE_FROM_CART", payload: item.product.id })
  }


  return (
    <>
      <div class="wrapper" >

        {
          products.map(item => {
            return (
              <>
                <div class="grid">
                  <div class="photo"><img src={item.image} alt="" /></div>
                  <div>{item.title}</div>
                  <div><b>{item.price} $</b></div>
                  <div> How many?  <input class="inputHome" type="number" onChange={e => onChange(e)} /></div> <br />
                  <div> <button class="button1" onClick={() => byAmount(item)}>Add to cart</button>   </div>

                </div>


              </>
            )
          })
        }


      </div>
      <Bask minOne={removeItem} addOne={addItem} delete={deleteItem} />
      <Warn is={warn} setCl={setWarn} />


    </>
  );

}

// {
//   if (open) {
//     return (
//       <Bask />
//     )
//   }
// }

export default App;

// class ExampleApp extends React.Component {
//   constructor () {
//     super();
//     this.state = {
//       showModal: false
//     };

//     this.handleOpenModal = this.handleOpenModal.bind(this);
//     this.handleCloseModal = this.handleCloseModal.bind(this);
//   }

//   handleOpenModal () {
//     this.setState({ showModal: true });
//   }

//   handleCloseModal () {
//     this.setState({ showModal: false });
//   }

//   render () {
//     return (
//       <div>
//         <button onClick={this.handleOpenModal}>Trigger Modal</button>
//         <ReactModal 
//            isOpen={this.state.showModal}
//            contentLabel="Minimal Modal Example"
//         >
//           <button onClick={this.handleCloseModal}>Close Modal</button>
//         </ReactModal>
//       </div>
//     );
//   }
// }

// const props = {};

// ReactDOM.render(<ExampleApp {...props} />, document.getElementById('main'))